from lxml import html
import requests
web_link = 'http://www.snopes.com/russian-spy-ship-east-coast/'
def fact_check(web_link):
    page = requests.get(web_link)
    tree = html.fromstring(page.content)
    truth = tree.xpath(".//main[@class='main']/section[@class='main-content-area has-sidebar']/div[@class='main-content-wrapper clearfix']/div[@class='body-content']/article/div[@class='entry-content article-text legacy']/div[@class=('claim true' or 'claim false')]/span/text()")
    return truth

def get_source(web_link):
    page = requests.get(web_link)
    tree = html.fromstring(page.content)
    source = tree.xpath(".//main[@class='main']/section[@class='main-content-area has-sidebar']/div[@class='main-content-wrapper clearfix']/div[@class='body-content']/article/div[@class='entry-content article-text legacy']/footer[@class='article-footer']/div[@class='article-sources-box']/p/text() | .//main[@class='main']/section[@class='main-content-area has-sidebar']/div[@class='main-content-wrapper clearfix']/div[@class='body-content']/article/div[@class='entry-content article-text legacy']/footer[@class='article-footer']/div[@class='article-sources-box']/p/em/text()")
    headlines_info = []
    if len(source) >0:
        data = []
        for line in source:
            text = list(filter(lambda x: x != '' and x != ' ' and x != '   ',line.replace('\n','\xa0').split('\xa0')))
            if len(text) == 0:continue
            for t in text:
                t = t.strip('\n')
                if len(t) == 0: continue
                #check if the first and last character is a quotation mark
                if t[0] == chr(8220) and t[-1] == chr(8221): 
                    data.append(("*HEADLINE*",t[1:-1]))
                    break
                if t[0].isdigit() and t[-2].isdigit(): 
                    data.append(("*DATE*",t))
                    headlines_info.append((data))
                    data = []
                    break
                else:
                    data.append(t)
    return headlines_info


from bs4 import BeautifulSoup
import urllib.request
url = "http://www.snopes.com/category/facts/"
def get_links(url):
    all_links = []
    for i in range(575):
        resp = urllib.request.urlopen(url)
        soup = BeautifulSoup(resp, from_encoding=resp.info().get_param('charset'))
        links = []
        flag = False
        for link in soup.find_all('a', href=True):
            if link["href"] == '#grid':
                flag = True
                continue
            if link["href"] == "http://www.snopes.com/": flag = False
            if flag == False: continue
            links.append(link["href"])
        url = links[-1]
        all_links += links[:-2]
    return all_links
