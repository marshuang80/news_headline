import numpy as np 
#matplotlib no module name 'PyQt4'****
import matplotlib.pyplot as plt 
from keras.models import Sequential
from keras.layers.core import Dense,Activatoin,Dropout
from keras.laters.recurrent import LSTM

#Set random seed for constant use
np.random.seed(1111)

#Code for reading data ****Still waiting for headline data


##The RNN
#Building the model
def build_model():
    model = Sequential()
    #1D input and output layer, with 2 100D hidden layer
    layers = [1,100,100,1]
    model.add(LSTM(input_dim=layers[0],output_dim=layers[1],return_sequences=True))
    model.add(Dropout(0.2))
    #return_sequnce = false: many to one output style 
    model.add(LSTM(output_dim=layers[2],return_sequences=False))
    model.add(Dropout(0.2))
    model.add(Dense(output_dim= layers[3]))
    model.add(Activation("linear"))
    return model
rnn = build_model()
rnn.compile(loss='mse',optimizer='rmsprop')
def run_network(model,data):
    #x_train,y_train = data .....
    epochs = 100
    model.fit(x_train,y_train,batch_size=512,nb_epoch = epochs,validation_split=0.10)
    predicted = model.predict(x_test)
    return model, predicted
def plot_predictions(y_test,prediction):
    fig = plt.figure() 
    ax = fig.add_subplot(111)
    ax.plot(y_test)
    plt.plot(predicted)
    plt.savefig('pred.png')


